﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Main class in the game. Provides global access poing to all
/// other managers and at the same time it's a 'entrance point'
/// for Uniti engine (Update and OnGUI events). Sets up game
/// when started. Organized as singleton (slightly modified
/// because of MonoBehaviour inheritance).
/// Should be on the scene in one exemplar
/// </summary>
public class SceneManager : MonoBehaviour
{	
	/* 
	 * SceneManager is set as a singleton so all public fields
	 * and functions are declared as static
	 */
	//Rect with camera bounds in units (used for circles' positioning)
	public static Rect ScreenBounds{ get; private set; }
	//Length of one pixel on the screen in units
	public static float PixelLength{ get; private set; }
	//Bunch of links to other manager to provide global access point
	public static CircleManager CircleManager{ get; private set; }
	public static LevelManager LevelManager{ get; private set; }
	public static ScoreManager ScoreManager{ get; private set; }
	public static TextureManager TextureManager{ get; private set; }
	//Whether the game is paused or not
	public static bool Paused { get; private set; }
	
	//Bundle, loaded from www, made public for access from coroutine (out)
	public static AssetBundle Bundle{ get; private set; }
		
	//Path to the bundle to load (configured from Unity editor)
	public string BundlePath;
	//Link to messenger game object's script (provided through editor)
	public Messenger messenger;
	
	//Singleton link
	private static SceneManager onlyOneSceneManager = null;
	
	/// <summary>
	/// Starts the game:
	/// Loads resources from bundle, sets up all the managers,
	/// tels all the managers to start the game.
	/// </summary>
	/// <returns>Made IEnumerator to be able to call coroutines</returns>
	public IEnumerator Start()
	{
		enabled = false;			//Disable updates while loading
		//Display loading message
		messenger.DisplayMessage("Loading...\n\nFeatures:"
			+ "\nDifferent circles"
			+ "\nScore system"
			+ "\nAnimations"
			+ "\nSound effects"
			+ "\nAsset bundle loading"
			+ "\nReal-time texture generating"
			+ "\nGame level encreases each 10 seconds"
			+ "\nMemory managing"
			+ "\n[Space] - pause the game"
			+ "\n\nCreated by Dmitriy Andreev"
			+ "\nSaint-Petersburg 08 Jan 2014");
		//If there is another SceneManager started then DIE (singleton violation, fix the scene!)
		if (onlyOneSceneManager != null)
			throw new System.Exception("More than one SceneManager on the scene!");
		onlyOneSceneManager = this;
		SetScreenParameters();		//Evaluate camera position and screen resolution
		//Load resource bundle in coroutine
		yield return StartCoroutine(LoadBundle(BundlePath));
		//Creage managers
		LevelManager = new LevelManager();
		TextureManager = new TextureManager();
		//ScoreManager have to load font from the bundle
		ScoreManager = new ScoreManager(Bundle);
		//CircleManager have to load CircleHolder object from the bundle
		CircleManager = new CircleManager(Bundle);
		SetupBackground(Bundle);	//Background object is also loaded from the bundle
		Bundle.Unload(false);		//Free resources
		Destroy(Bundle);
		Debug.Log(Time.time.ToString("0.00") + ": Bundle disposed");
		//Wait for the first texture set being generated
		while (!TextureManager.NextSetIsReady)
			yield return null;
		enabled = true;				//Loading completed, enable updates
		Debug.Log(Time.time.ToString("0.00") + ": Game started");
		messenger.DisplayMessage();	//Clear loading message from the screed
		//Tell other managers the game was started
		ScoreManager.StartGame();
		LevelManager.StartGame();
	}
	
	/// <summary>
	/// Loads the bundle from path specified and place it in
	/// Bundle global variable
	/// </summary>
	/// <returns>Made IEnumerator to be called as coroutine</returns>
	/// <param name="bundlePath">Bundle path</param>
	private IEnumerator LoadBundle(string bundlePath)
	{
		//Get rid of relative paths
		string assetPath = Application.dataPath + bundlePath;
		assetPath = "file://" + assetPath;
		Debug.Log(Time.time.ToString("0.00") + ": Bundle loading started from " + assetPath);
		/*
		 * It's recommended to use caching system and
		 * LoadFromCacheOrDownload function for bundle download, but
		 * to avoid necessity to change code each time bundle file
		 * was changed it's replaced by forsed download.
		 * To make it right uncomment two lines beneath and delete
		 * using line
		 */
		//while (!Caching.ready) yield return null;
		//using (WWW www = WWW.LoadFromCacheOrDownload(assetPath, 1))
		using (WWW www = new WWW(assetPath))
		{
			yield return www;
			Bundle = www.assetBundle;
		}
		Debug.Log(Time.time.ToString("0.00") + ": Bundle loading completed");
	}
	
	/// <summary>
	/// Executes the provided function as coroutine.
	/// Introduced to allow other managers (classes which are not 
	/// derived from MonoBehaviour) start coroutines
	/// </summary>
	/// <param name="function">Function to run as coroutine</param>
	public static void ExecuteInBackground(IEnumerator function)
	{
		onlyOneSceneManager.StartCoroutine(function);
	}
	
	/// <summary>
	/// Checks whether pause key (space) is pressed and invoke
	/// non-MonoBehaviour managers' update
	/// </summary>
	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Paused = !Paused;
			if (Paused)
			{	//If the game have been paused
				Time.timeScale = 0f;	//Freeze game time
				//Display pause message
				messenger.DisplayMessage("Paused\nPress [Space] to continue");
			}
			else
			{	//If the game have been unpaused
				Time.timeScale = 1f;	//Continue game
				//Clear the pause message
				messenger.DisplayMessage();
			}
		}
		if (!Paused)					//Invoke other managers' update if not paused
			LevelManager.Update();
	}
	
	/// <summary>
	/// Displays score and time (through call to ScoreManager).
	/// Provides OnGUI event to non-MonoBehaviour managers
	/// </summary>
	public void OnGUI()
	{
		ScoreManager.ShowGUI();
	}
	
	/// <summary>
	/// Evaluates camera position and screen resolution to define
	/// camera bounds (in units) and save them to ScreenBounds
	/// and sets PixelLength
	/// </summary>
	private void SetScreenParameters()
	{
		//Find main camera location on the scene
		Camera camera = (GameObject.FindGameObjectWithTag("MainCamera") as GameObject).GetComponent<Camera>();
		Vector2 camPos = camera.transform.position;
		//Calculate camera size in units
		float camHeight = camera.orthographicSize * 2f;
		float aspectRatio = (float)Screen.width / Screen.height;
		float camWidth = camHeight * aspectRatio;
		//Set ScreenBounds
		float left = camPos.x - camWidth * 0.5f;
		float bottom = camPos.y - camHeight * 0.5f;
		ScreenBounds = new Rect(left, bottom, camWidth, camHeight);
		
		PixelLength = camHeight / Screen.height;
	}
	
	/// <summary>
	/// Loads background object from bundle provided and place
	/// it on the scene
	/// </summary>
	/// <param name="bundle">Bundle with Background game object</param>
	private void SetupBackground(AssetBundle bundle)
	{
		GameObject back = bundle.Load("Background", typeof(GameObject)) as GameObject;
		GameObject.Instantiate(back);
	}
}
