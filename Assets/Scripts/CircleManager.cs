﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class for creating, tracking and texturizing (through TextureManager)
/// all circles in the game
/// </summary>
public class CircleManager
{
	private GameObject circlePrefab;            //Prefab for circles instantiation
	private List<CircleBehavior> circles;       //list of all (generated) circles on the scene
	private static int nextSortingOrder = 0;    //last occupied sorting order (introduced for prevent circle blinking)
	
	/// <summary>
	/// Constructor which loads CircleHolder prefab from bundle
	/// provided as a parameter
	/// </summary>
	/// <param name="bundle">Asset bundle with CircleHolder game object</param>
	public CircleManager(AssetBundle bundle)
	{
		//Synchronious loading is used because yield is not avaliable in constructors
		GameObject circle = bundle.Load("CircleHolder", typeof(GameObject)) as GameObject;
		circlePrefab = circle;
		circles = new List<CircleBehavior>();
	}
	
	/// <summary>
	/// Stop tracking the circle and remove it from the list
	/// </summary>
	/// <param name="circle">Circle to delete from list</param>
	public void ForgetCircle(CircleBehavior circle)
	{
		circles.Remove(circle);
	}
	
	/// <summary>
	/// Texturize all the circles with current textures from
	/// TextureManager.
	/// Get called by TextureManager right after setting next
	/// generated texture set as current
	/// </summary>
	public void UpdateCirclesTextures()
	{
		foreach (CircleBehavior circle in circles)
		{
			SceneManager.TextureManager.SetTextureToCircle(circle);
		}
	}
	
	/// <summary>
	/// Instantiate one circle on the top line of the screen
	/// </summary>
	/// <param name="size">New circle size</param>
	public void GenerateCircle(float size)
	{
		/*
         * Random x coordinate generated (taking circle size into
         * account) and y coordinate calculated from the camera
         * bounds stored in SceneManager
         */
		float x = Random.Range(SceneManager.ScreenBounds.xMin + size, SceneManager.ScreenBounds.xMax - size);
		float y = SceneManager.ScreenBounds.yMax - size;
		//Instantiate new CircleHolder
		GameObject circleGO = GameObject.Instantiate(circlePrefab, new Vector2(x, y), new Quaternion()) as GameObject;
		//Place it on the next sorting layer to prevent blinking while circle intersection
		circleGO.GetComponentInChildren<SpriteRenderer>().sortingOrder = nextSortingOrder++;
		CircleBehavior circle = circleGO.GetComponent<CircleBehavior>();
		//Setup link to CircleManager for further ForgetCircle function call
		circle.CircleManager = this;
		circle.Size = size;
		circles.Add(circle);
	}
	
}
