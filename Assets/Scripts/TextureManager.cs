﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Class that provides texture sets' generation (as coroutine through
/// the SceneManager). All textures are images of fractal Julia set
/// in complex plane for polynomial complex function Z^2+C
/// </summary>
public class TextureManager
{
	//Whether the next texture set is generated or not. Made public for coroutine access
	public bool NextSetIsReady { get; private set; }
	
	//Number of fractal points to process each frame
	private int yieldFactor = 900;			//Overall complexity is about 69000 iterations
	//Number of iterations to evaluate wether the point is in Julia set
	private int juliaSetEvaluationDepth = 60;
	//Complex parameter for Julia set computation
	private float juliaSetCRe;              //Real component
	private float juliaSetCIm;              //Imagenary component
	
	//Dictionary for storing currently used texture set
	private Dictionary<TextureSize, Texture2D> currentTextureSet;
	//Dictionary for generating and storing next texture set
	private Dictionary<TextureSize, Texture2D> nextTextureSet;
	
	/// <summary>
	/// Constuctor that launches firs texture set generation
	/// </summary>
	public TextureManager()
	{
		NextSetIsReady = false;             //Texture set is not generated yet
		Debug.Log(Time.time.ToString("0.00") + ": First texture set generation started");
		//Start first texture set generation
		SceneManager.ExecuteInBackground(GenerateNextTextureSet(true));
	}
	
	/// <summary>
	/// Set texture with corresponding resolution to the circle
	/// </summary>
	/// <param name="circle">Circle to texturize</param>
	public void SetTextureToCircle(CircleBehavior circle)
	{
		//Find best fitting texture size
		TextureSize size = EvaluateCircleSize(circle);
		//Texturize
		circle.SetTexture(currentTextureSet[size]);
	}
	
	/// <summary>
	/// Coroutine procedure for next level texture set generation.
	/// Gradually produces allowed by TextureSize enum textures into
	/// nextTextureSet dictionary, interrupting execution for a
	/// frame each yieldFactor iterations.
	/// If it's the first texture generation then it automatically
	/// launches next texture set generation after completeon
	/// </summary>
	/// <param name="first">Whether it's the first generation or not</param>
	/// <returns>Enumerator for coroutine-execution</returns>
	private System.Collections.IEnumerator GenerateNextTextureSet(bool first = false)
	{
		yield return null;
		nextTextureSet = new Dictionary<TextureSize, Texture2D>();
		foreach (TextureSize ts in System.Enum.GetValues(typeof(TextureSize)).Cast<TextureSize>())
		{   //For all texture sizes from TextureSize enum
			RandomizeFractalParameters();   //Change fractal picture parameter for the sake of diversity
			int size = (int)ts;
			//New main saturated color for the texture about to generate
			Color leadColor = RGBfromHSV(Random.Range(0f, 1f), 1f, 1f);
			Texture2D texture = new Texture2D(size, size, TextureFormat.ARGB32, false);
			//Array of pixels to apply to the texture
			Color[] col = new Color[size * size];
			int i = 0;                      //Index in that array
			int radius = size * size / 4;   //Radius of the circle in pixels
			float scale = 1.5f / size;      //Scale for fractal imageing
			int calculationsDone = 0;       //Fractal iterations counter
			for (int x = -size/2+1; x <= size/2; x++)
			{
				for (int y = -size/2+1; y <= size/2; y++)
				{   //For all the pixels in the texture
					i++;
					if (x * x + y * y > radius)
						//If the pixel is out of circle then leave it transparent
						continue;
					//Get brightness of the pixel from fractal function
					float jsBrightness = JuliaSetBrigthness(x * scale, y * scale);
					col[i].a = 1f;
					col[i].r = leadColor.r * jsBrightness;
					col[i].g = leadColor.g * jsBrightness;
					col[i].b = leadColor.b * jsBrightness;
					calculationsDone++;
				}
				if (calculationsDone > yieldFactor)
				{   //If we made too much work for a frame then stop for a frame
					calculationsDone = 0;
					yield return null;
				}
			}
			texture.SetPixels(col);         //Apply generated pixel set to texture
			texture.Apply();
			nextTextureSet.Add(ts, texture);//Add result to the dictionary
		}
		NextSetIsReady = true;              //Flag that calculations completed
		Debug.Log(Time.time.ToString("0.00") + ": Texture set generated");
		if (first)
			//If it was generation of the first texture set then apply them and start over
			ChangeTextureSetToNext();
	}
	
	/// <summary>
	/// Sets generated texture set as the current one and launches next
	/// texture set generation in background
	/// </summary>
	public void ChangeTextureSetToNext()
	{
		if (!NextSetIsReady)
			//If the next texture set is not ready then DIE
			throw new System.Exception("Texture set is not ready!");
		if (currentTextureSet != null)
		{   //Dispose resources from the previous texture set
			foreach (Texture2D tex in currentTextureSet.Values)
				Object.Destroy(tex);
			currentTextureSet.Clear();
		}
		currentTextureSet = nextTextureSet;     //Set link to the new set
		Debug.Log(Time.time.ToString("0.00") + ": Texture sets swapped, next set generation started");
		//Apply next set of textures to all the circles in the scene
		SceneManager.CircleManager.UpdateCirclesTextures();
		NextSetIsReady = false;					//Next generation is about to begin, drop readiness flag
		//Run next texture set generation in background immediately
		SceneManager.ExecuteInBackground(GenerateNextTextureSet());
	}
	
	/// <summary>
	/// Evaluates circle size and finds the best fitting texture
	/// resolution from existing in the TextureSize enum
	/// </summary>
	/// <param name="circle">The circle which size to evaluate</param>
	/// <returns>Enum from TextureSize</returns>
	private TextureSize EvaluateCircleSize(CircleBehavior circle)
	{
		//Calculates how many pixels the circle occupies on the screen
		int pixelsInCircle = (int)System.Math.Round(2f * circle.Size / SceneManager.PixelLength);
		//In the next 7 lines we find the largest power of 2 smaller than pixelsInCircle
		int powerOf2 = 0;
		while (pixelsInCircle>0)
		{
			pixelsInCircle >>= 1;
			powerOf2++;
		}
		pixelsInCircle = 1 << powerOf2;
		//If we are out of allowed by TextureSize range then select the apropriate option
		if (pixelsInCircle < 32)
			pixelsInCircle = 32;
		if (pixelsInCircle > 256)
			pixelsInCircle = 256;
		return (TextureSize)pixelsInCircle;
	}
	
	/// <summary>
	/// Calculates brightness of the point (x,y) in the complex
	/// plane depending on it's remoteness from Julia set
	/// fractal
	/// </summary>
	/// <param name="x">Real component of the point in the complex plane</param>
	/// <param name="y">Imagenary component of the point in the complex plane</param>
	/// <returns>Brightness from [0,1] range</returns>
	private float JuliaSetBrigthness(float x, float y)
	{
		float xTemp;
		for (int i = 0; i < juliaSetEvaluationDepth; i++)
		{
			/*
             * Here we repeat calculation for complex point Z
             * Z = Z^2 + C
             * until it goes out of circle with radius 10 and
             * center in the origin, which means it's not belongs
             * to the Julia set
             */
			xTemp = x * x - y * y + juliaSetCRe;
			y = 2 * x * y + juliaSetCIm;
			x = xTemp;
			if (x * x + y * y > 100f)
				// The faster we dropped out of the circle - the darker the point
				return i / (float)juliaSetEvaluationDepth;
		}
		//If after all the iterations we are still in the circle
		//with radius 10 then probably the point is in the Julia set
		return 1f;
	}
	
	/// <summary>
	/// Sets random fractal parameter C which is stored in 
	/// juliaSetCRe (real component) and juliaSetCIm (imagenary
	/// component) private variables
	/// </summary>
	private void RandomizeFractalParameters()
	{
		/* 
         * There are four different regions on the complex plane
         * for C parameter which give good-looking fractal images
        */ 
		int setNumber = Random.Range(0, 3);
		switch (setNumber)
		{
			case 1:         //Region around point 0.285-0.01i
				juliaSetCRe = Random.Range(0.284f, 0.286f);
				juliaSetCIm = Random.Range(-0.011f, -0.009f);
				break;
			case 2:         //Region around point -0.7+0.385i
				
				juliaSetCRe = Random.Range(-0.731f, -0.677f);
				juliaSetCIm = Random.Range(0.356f, 0.414f);
				break;
			case 3:         //Region around point 0.280+0.011i
				juliaSetCRe = Random.Range(0.279f, 0.281f);
				juliaSetCIm = Random.Range(0.011f, 0.012f);
				break;
			default:         //Region around point -0.8+0.155i
				juliaSetCRe = Random.Range(-0.805f, -0.795f);
				juliaSetCIm = Random.Range(0.152f, 0.157f);
				break;
		}
	}
	
	/// <summary>
	/// Converts color from Hue-Saturation-Value space to
	/// ordinary RGB color.
	/// Introduced to avoid using Editor script
	/// Unity.Editor.EditorGUIUtility.HSVToRGB
	/// </summary>
	/// <param name="h">Hue</param>
	/// <param name="s">Saturation</param>
	/// <param name="v">Value</param>
	/// <returns>Color in convenient RGB format</returns>
	private static Color RGBfromHSV(float h, float s, float v)
	{
		//The algorithm was taken from http://www.cs.rit.edu/~ncs/color/
		int i;
		float f, p, q, t;
		h *= 6f;			
		i = (int)h;     // Hue sector 0..5
		f = h - i;	    // Factorial part of Hue
		p = v * (1f - s);
		q = v * (1f - s * f);
		t = v * (1f - s * (1f - f));
		switch (i)
		{
			case 0:
				return new Color(v, t, p);
			case 1:
				return new Color(q, v, p);
			case 2:
				return new Color(p, v, t);
			case 3:
				return new Color(p, q, v);
			case 4:
				return new Color(t, p, v);
			default:
				return new Color(v, p, q);
		}
	}
	
	/// <summary>
	/// Enumerator with supported texture sizes
	/// </summary>
	private enum TextureSize
	{
		square32 = 32,
		square64 = 64,
		square128 = 128,
		square256 = 256,		
	}
}
