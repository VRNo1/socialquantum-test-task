﻿using UnityEngine;

/// <summary>
/// Class that tracks all game statistics and display in on
///	a GUI texture
/// </summary>
public class ScoreManager
{
	public int Score = 0;           //Player's score
	public int CirclesMissed = 0;   //How many circles reached the bottom line
	//How much time past since the game start
	public float GameDuration{ get { return Time.time - startTime; } }
	
	private float startTime;        //Time of the beginning of the game (not application start)
	private GUIStyle style;         //Style for score and time lable displaying
	
	/// <summary>
	/// Constructor which loads the required font from the bundle
	/// provided as a parameter and configures the style of the
	/// score lable
	/// </summary>
	/// <param name="bundle">Asset bundle with BOOKOS font</param>
	public ScoreManager(AssetBundle bundle)
	{
		style = new GUIStyle();
		style.font = bundle.Load("BOOKOS", typeof(Font)) as Font;
		style.fontSize = 17;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = new Color(1f, 1f, 1f);
	}
	
	/// <summary>
	/// Zeroizes the score and the timer at the game start.
	/// Get called by the SceneManager when all the resources
	/// are loaded
	/// </summary>
	public void StartGame()
	{
		startTime = Time.time;
		Score = 0;
	}
	
	/// <summary>
	/// Display score and timer lable.
	/// Get called by SceneManager in it's OnGUI function to
	/// get around the necessity to inherit ScoreManager from 
	/// MonoBehaviour
	/// </summary>
	public void ShowGUI()
	{
		GUI.Label(new Rect(0, Screen.height - 60, 150, 60),
		          "Score: " + Score
			+ "\nTime: " + GameDuration.ToString("0.0")
			+ "\nCircles Missed: " + CirclesMissed,
		          style);
	}
}
